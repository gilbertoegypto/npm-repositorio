let a = { index: 0, descricao: 'a - zero', passou: false, proximo: undefined };
let b = { index: 1, descricao: 'b - um', passou: false, proximo: undefined };
let c = { index: 2, descricao: 'c - dois', passou: false, proximo: undefined };
let d = { index: 3, descricao: 'd - tres', passou: false, proximo: undefined };
let e = { index: 4, descricao: 'e - quatro', passou: false, proximo: undefined };
let f = { index: 5, descricao: 'e - cinco', passou: false, proximo: undefined };

let primeiroPacote = function () {
      a.proximo = [b, c, d];
      b.proximo = [a, c];
      c.proximo = [a, b, e];
      d.proximo = [a];
      e.proximo = [c];
      return a;
}

let segundoPacote = function () {
      a.proximo = [b, e];
      b.proximo = [c, d, f];
      c.proximo = undefined;
      d.proximo = [f];
      e.proximo = undefined;
      return a;
}

let terceiroPacote = function () {
      a.proximo = [c, e];
      b.proximo = [d, f];
      c.proximo = [b];
      d.proximo = [f,e];
      e.proximo = undefined;
      return a;
}

let resetPacote = function () {
    a.passou = false;
    b.passou = false;
    c.passou = false;
    d.passou = false;
    e.passou = false;
    f.passou = false;
    return a;
}


function dfs(pacote, lista) {
    if (pacote.passou) {
          return;
    }

    pacote.passou = true;

    lista.push(pacote);

    if (pacote.proximo) {
          pacote.proximo.forEach((proximoPacote) => {
                dfs(proximoPacote, lista);
          });
    }
}

let listaDePacotes = [];
  

function runCommandOnEnter(event) {
    if (event.key === 'Enter') {
      runCommand();
    }
  }

  function runCommand() {

    const input = document.getElementById('command-input');
    const words = input.value;

    if (words.toLowerCase() === 'npm install primeiropacote') {

        dfs(primeiroPacote(), listaDePacotes);
        printDependencie(input,words)
        
      } else if (words.toLowerCase() === 'npm install segundopacote') {

        dfs(segundoPacote(), listaDePacotes);
        printDependencie(input,words)
        
      } else if (words.toLowerCase() === 'npm install terceiropacote') {

        dfs(terceiroPacote(), listaDePacotes);
        printDependencie(input,words)
        
      } else if(words.toLowerCase() === 'clean'){
        resetPrompt()
        input.value = '';
      } else {
 
        writeDom(input, 'Invalid command', false)
        
        
      }

      

  }

  function printDependencie(input, words){
    let reversedlistaDePacotes = listaDePacotes.slice().reverse();

    writeDom(input, words, false)

    const delay = (ms) => new Promise(resolve => setTimeout(resolve, ms));

    const iterateReversedListaDePacotes = async () => {
        for (const dependencie of reversedlistaDePacotes) {
            writeDom(input, dependencie.descricao, true);
            await delay(500); 
        }
    };

    iterateReversedListaDePacotes().then(() => {
        resetPacote();
        listaDePacotes = []; 
    });
}

function resetPrompt() {
    const outputBox = document.getElementById('output-box');
    outputBox.innerHTML = ''; // Erase everything in the output box

}


  
  function writeDom(input, words, flagInstall){

    if(flagInstall){
        const outputBox = document.getElementById('output-box');
        const outputText = document.createElement('p');
        outputText.textContent = 'You installed: ' + words;
        outputBox.appendChild(outputText);
        
        input.value = '';
        input.focus();
    }else{
        const outputBox = document.getElementById('output-box');
        const outputText = document.createElement('p');
        outputText.textContent = 'You entered: ' + words;
        outputBox.appendChild(outputText);
        
        input.value = '';
        input.focus();
    }


  }
  
  