let a = { index: 0, descricao: 'a - zero', passou: false, proximo: undefined };
let b = { index: 1, descricao: 'b - um', passou: false, proximo: undefined };
let c = { index: 2, descricao: 'c - dois', passou: false, proximo: undefined };
let d = { index: 3, descricao: 'd - tres', passou: false, proximo: undefined };
let e = { index: 4, descricao: 'e - quatro', passou: false, proximo: undefined };
let f = { index: 5, descricao: 'e - cinco', passou: false, proximo: undefined };

let primeiroPacote = function () {
      a.proximo = [b, c, d];
      b.proximo = [a, c];
      c.proximo = [a, b, e];
      d.proximo = [a];
      e.proximo = [c];
      return a;
}

let segundoPacote = function () {
      a.proximo = [b, e];
      b.proximo = [c, d, f];
      c.proximo = undefined;
      d.proximo = [f];
      e.proximo = undefined;
      return a;
}

let terceiroPacote = function () {
      a.proximo = [c, e];
      b.proximo = [d, f];
      c.proximo = [b];
      d.proximo = [f,e];
      e.proximo = undefined;
      return a;
}


function dfs(pacote, lista) {
      if (pacote.passou) {
            return;
      }

      pacote.passou = true;

      lista.push(pacote);

      if (pacote.proximo) {
            pacote.proximo.forEach((proximoPacote) => {
                  dfs(proximoPacote, lista);
            });
      }
}

let listaDePacotes = [];

dfs(segundoPacote(), listaDePacotes);

console.log(listaDePacotes);